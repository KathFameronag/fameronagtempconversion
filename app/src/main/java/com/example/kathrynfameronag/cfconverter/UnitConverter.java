package com.example.kathrynfameronag.cfconverter;

/**
 * Created by Kathryn Fameronag on 14/07/2017.
 */

public class UnitConverter {
    public static double celsiusTofahrenheit(double f){
        return (f-32)*5/9;
    }

    public static double fahrenheitTocelsius(double c){
        return (c*9/5) + 32;
    }
}
